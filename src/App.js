import logo from './logo.svg';
import './App.css';
import { Routes, Route, Link } from 'react-router-dom';
import DefaultLayout from "./layoutes/DefaultLayout";
import Page1 from "./pages/Page1";
import Page2 from "./pages/Page2";
import Page3 from "./pages/Page3";
import Page4 from "./pages/Page4";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<DefaultLayout><Page1/></DefaultLayout>}/>
            <Route  path="/Page2" element={<DefaultLayout><Page2/></DefaultLayout>}/>
            <Route  path="/Page3" element={<DefaultLayout><Page3/></DefaultLayout>}/>
            <Route  path="/Page4" element={<DefaultLayout><Page4/></DefaultLayout>}/>
        </Routes>
    </div>
  );
}

export default App;
