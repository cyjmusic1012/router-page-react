import React, {useState} from 'react';

const Page1 = () => {

    const [manName, setManName] = useState('');
    const [womanName, setWomanName] = useState('');
    const [score, setScore] = useState(0);

    const hadleManName = e => {
        setManName(e.target.value);
    }

    const handleWomanName = e => {
        setWomanName(e.target.value);
    }

    const calculateScore = () => {
        setScore(Math.floor(Math.random()*101))
    }

    return(
        <div>
            <input type="text" value={manName} onChange = {hadleManName}/>
            <input type="text" value={womanName} onChange={handleWomanName}/>
            <button onClick={calculateScore}>버튼</button>
            <br/>
            {manName}와 {womanName}의 궁합도는 {score}% 입니다.
        </div>
    );
}
export default Page1;