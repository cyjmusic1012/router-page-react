import React from 'react';
import {Link} from "react-router-dom";


const DefaultLayout = ({ children }) => {

    return (
        <>
        <header>
            헤더입니다
        </header>
            <nav>
                <p><Link to="/">페이지</Link></p>
                <p><Link to="/Page2">페이지2</Link></p>
                <p><Link to="/Page3">페이지3</Link></p>
                <p><Link to="/Page4">페이지4</Link></p>
            </nav>
            <main>{children}</main>
            <footer>푸터입니다
            </footer>
        </>
    );
}
export default DefaultLayout;